var elixir = require('laravel-elixir'),
    liveReload = require('gulp-livereload'),//roda verificando alterações
    rm = require('gulp-rimraf'),//cria versão de arquivos
    gulp = require('gulp');
 
 //Definir os diretório
var config ={
  assets_path:'./resources/assets',
  build_path:'./public/build'
};

//Configuração do bower components path
config.bower_path = config.assets_path + '/../bower_components';

//Configuração de path js
config.build_path_js = config.build_path + '/js';
config.build_vendor_path_js = config.build_path_js + '/vendor';
config.vendor_path_js = [
    config.bower_path + '/jquery/dist/jquery.min.js',
    config.bower_path + '/bootstrap/dist/js/bootstrap.min.js',
    config.bower_path + '/angular/angular.min.js',
    config.bower_path + '/angular-route/angular-route.min.js',
    config.bower_path + '/angular-resource/angular-resource.min.js',
    config.bower_path + '/angular-animate/angular-animate.min.js',
    config.bower_path + '/angular-messages/angular-messages.min.js',
    config.bower_path + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
    config.bower_path + '/angular-strap/src/navbar/navbar.js',
    config.bower_path + '/angular-cookies/angular-cookies.min.js',
    config.bower_path + '/query-string/query-string.js',
    config.bower_path + '/angular-oauth2/dist/angular-oauth2.min.js',
    config.bower_path + '/ng-file-upload/ng-file-upload.min.js',
    config.bower_path + '/AngularJS-Toaster/toaster.min.js',
    config.bower_path + '/angular-http-auth/src/http-auth-interceptor.js',
];

//Configuração de path css
config.build_path_css = config.build_path + '/css';
config.build_vendor_path_css = config.build_path_css + '/vendor';
config.vendor_path_css = [
    config.bower_path + '/bootstrap/dist/css/bootstrap.min.css',
    config.bower_path + '/bootstrap/dist/css/bootstrap-theme.min.css',
    config.bower_path + '/AngularJS-Toaster/toaster.min.css',
];

config.build_path_html = config.build_path + '/views';
config.build_path_font = config.build_path + '/fonts';
config.build_path_images = config.build_path + '/images';

//Tarefa para copia font
gulp.task('copy-fonts', function(){
     gulp.src([
        config.assets_path + '/fonts/**/*'
    ]).pipe(gulp.dest(config.build_path_font))
      .pipe(liveReload());
});
//Tarefa para copia images
gulp.task('copy-images', function(){
     gulp.src([
        config.assets_path + '/images/**/*'
    ]).pipe(gulp.dest(config.build_path_images))
      .pipe(liveReload());
});

//Tarefa para copiar javascript
gulp.task('copy-scripts', function(){
    //gulp.src - recupera o caminhos dos css
    //gulp.dest- informa a pasta do destinos dos css
    //pipe - executa tarefas
    gulp.src([
        //dois asteriscos indica que vai procurar em todas as pastar
        //um asterisco indica que vai pegar qualquer arquivo .js
        config.assets_path + '/js/**/*.js'
    ]).pipe(gulp.dest(config.build_path_js))
      .pipe(liveReload());
      
      //script de terceiros
      gulp.src(config.vendor_path_js)
        .pipe(gulp.dest(config.build_vendor_path_js))
        .pipe(liveReload());
});

//Tarefa para copiar style
gulp.task('copy-styles', function(){
    //gulp.src - recupera o caminhos dos css
    //gulp.dest- informa a pasta do destinos dos css
    //pipe - executa tarefas
    gulp.src([
        //dois asteriscos indica que vai procurar em todas as pastar
        //um asterisco indica que vai pegar qualquer arquivo .css
        config.assets_path + '/css/**/*.css'
    ]).pipe(gulp.dest(config.build_path_css))
      .pipe(liveReload());
      
      //style de terceiros
      gulp.src(config.vendor_path_css)
        .pipe(gulp.dest(config.build_vendor_path_css))
        .pipe(liveReload());
});


//Tarefa para copiar html
gulp.task('copy-html', function(){
    gulp.src([
        config.assets_path + '/js/views/**/*.html'
    ]).pipe(gulp.dest(config.build_path_html))
      .pipe(liveReload());
      
});

//Tarefa de limpeza de diretório
gulp.task('clean', function(){
    return gulp.src(config.build_path).pipe(rm());
});

//Gerar arquivo para produção
// assets_path no final permite a diferenciação dos arquivos concatenados
//Tarefa passada em array [clean] é para ser executada antes das tarefas abaixo
gulp.task('default', ['clean'], function(){
    gulp.start('copy-html', 'copy-fonts', 'copy-images');
    
    elixir(function(mix){
       
        //Minifica arquivo css
       mix.styles(config.vendor_path_css.concat([config.assets_path +'/css/**/*.css']),
       'public/css/all.css', config.assets_path); 
       
       //Minifica arquivo js
       mix.scripts(config.vendor_path_js.concat([config.assets_path +'/js/**/*.js']),
       'public/js/all.js', config.assets_path); 
       
       //Versiona os arquivos para que o navegador não usa o que tá no cache
       mix.version(['js/all.js','css/all.css']);
    });
});

//Tarefa de ouvinte de modificação nos arquivos durante desenvolvimento
//Tarefa passada em array [clean] é para ser executada antes das tarefas abaixo
gulp.task('watch-dev', ['clean'], function(){
    liveReload.listen();
    
    //inicializa tarefas
    gulp.start('copy-styles', 'copy-scripts', 'copy-html', 'copy-fonts', 'copy-images');
    
    /*ouvintes de mudanças- verifica no assets_paht em todos os diretório as 
     mudanças. Caso ouver, executa o copy-styles e copy-scripts 'copy-fonts', 'copy-images'*/
    gulp.watch(config.assets_path + '/**', ['copy-styles', 'copy-scripts', 'copy-html']);
});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 

elixir(function(mix) {
    mix.sass('app.scss');
});
*/
