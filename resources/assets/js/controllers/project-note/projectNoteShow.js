angular.module('app.controllers')
        .controller('ProjectNoteShowController', ['$scope', 'Client', '$location', function($scope, Client, $location){
                $scope.client = new Client();
                
                $scope.save = function(){
                    if($scope.frmNew.$valid){
                        $scope.client.$save().then(
                            function(){
                                $location.path('/clients');
                            }
                        );
                    }
                };
        }]);
