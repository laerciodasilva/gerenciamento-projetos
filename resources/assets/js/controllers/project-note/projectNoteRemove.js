angular.module('app.controllers')
        .controller('ProjectNoteRemoveController', 
            ['$scope', '$location', '$routeParams','ProjectNote', 'toaster',
                function($scope, $location, $routeParams, ProjectNote, toaster){
                    ProjectNote.get({id: $routeParams.id, idNote: $routeParams.idNote}, function(data){
                        if (data.error) {
                            toaster.pop(data.type, "Mensagem", data.message);
                        }
                        $scope.projectNote =  data;
                    });
                    $scope.remove = function(){
                      $scope.projectNote.$delete({
                          id:$routeParams.id, idNote:$scope.projectNote.id
                      }).then(function(data){
                           toaster.pop(data.type, "Mensagem", data.message);
                          $location.path('/project/'+$routeParams.id +'/notes');
                      });  
                    };
        }]);

