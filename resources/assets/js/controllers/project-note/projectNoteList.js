angular.module('app.controllers')
        .controller('ProjectNoteListController', ['$scope', '$routeParams', 'ProjectNote', function($scope, $routeParams, ProjectNote){
                $scope.error ={
                    message: '',
                    error: false
                };
                $scope.project_id = $routeParams.id;
                $scope.projectNotes=ProjectNote.query({id: $routeParams.id},
                    function(){
                        
                    },function(data){
                        $scope.error.error = true;
                        $scope.error.message = data.data.error_description;
                    }
                );
        }]);