angular.module('app.controllers')
        .controller('ProjectNoteNewController', ['$scope', 'ProjectNote', 
                    '$routeParams', '$location', 'toaster',
                    function($scope, ProjectNote, $routeParams, $location, toaster){
                        $scope.projectNote = new ProjectNote();
                        $scope.save = function(){
                            if($scope.frmNew.$valid){
                                $scope.projectNote.$save({id: $routeParams.id}).then(function(data){
                                        toaster.pop(data.type, "Mensagem", data.message);
                                        $location.path('/project/'+$routeParams.id +'/notes');
                                    },function(data){
                                        toaster.pop(data.type, "Mensagem", data.message);
                                        $location.path('/project/'+$routeParams.id +'/notes');
                                });
                            }
                        };
        }]);
