angular.module('app.controllers')
        .controller('ProjectTaskEditController', 
                ['$scope', '$location', '$routeParams', 'ProjectTask', 'appConfig','toaster', 
                    function($scope, $location, $routeParams, ProjectTask, appConfig, toaster){        
                        ProjectTask.get({id:$routeParams.id, taskId: $routeParams.taskId}, function(data){
                            if (data.error) {
                                toaster.pop(data.type, data.title, data.message);
                            }
                            $scope.projectTask = data;
                        });
                        $scope.status = appConfig.projectTask.status;
                        $scope.edit = function(){
                            ProjectTask.update({id: $routeParams.id, taskId:$routeParams.taskId}, $scope.projectTask, 
                                function(data){
                                    toaster.pop(data.type, data.title, data.message);
                                   $location.path('/project/'+$routeParams.id+'/task'); 
                                },function(data){
                                    toaster.pop(data.type, data.title, data.message);
                                }
                            );
                        };
                        
                        $scope.dueDate = {
                            status: {
                                opened: false
                            }
                        };
                        $scope.startDate = {
                            status: {
                                opened: false
                            }
                        };
                        
                        $scope.openDueDate = function(){
                            $scope.dueDate.status.opened = true;
                        };
                        $scope.openStartDate = function(){
                            $scope.startDate.status.opened = true;
                        };
        }]);