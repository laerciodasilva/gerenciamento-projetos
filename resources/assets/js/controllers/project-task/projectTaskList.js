angular.module('app.controllers')
        .controller('ProjectTaskListController', ['$scope', 'ProjectTask', 
                    '$routeParams', 'toaster', 'appConfig',
                    function($scope, ProjectTask, $routeParams, toaster, appConfig){
                        $scope.projectTask = new ProjectTask();
                        
                        $scope.save = function(){
                            if($scope.form.$valid){
                                $scope.projectTask.status = appConfig.projectTask.status[0].value;
                                $scope.projectTask.$save({id: $routeParams.id}).then(function(data){
                                        $scope.projectTask = new ProjectTask();
                                        $scope.loadTask();
                                        toaster.pop(data.type, data.title, data.message);
                                    },function(data){
                                        toaster.pop(data.type, data.title, data.message);
                                });
                            }
                        };
                        
                        $scope.loadTask = function() {
                            $scope.projectTasks = ProjectTask.query({
                                id: $routeParams.id,
                                orderBy: 'id',
                                sortBy: 'desc'
                            
                            });
                        };
                        $scope.loadTask();
        }]);