angular.module('app.controllers')
        .controller('ClientRemoveController', 
            ['$scope', '$location', '$routeParams','Client', 'toaster', 
                function($scope, $location, $routeParams, Client, toaster){
                  Client.get({id: $routeParams.id}, function(data){
                    $scope.client = data;
                    if (data.error) {
                        toaster.pop(data.type, "Mensagem", data.message);
                    }
                });
                $scope.remove = function(){
                  $scope.client.$delete().then(function(data){
                     toaster.pop(data.type, "Mensagem", data.message);
                     $location.path('/clients');
                    });
                };
        }]);

