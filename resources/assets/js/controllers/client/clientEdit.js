angular.module('app.controllers')
        .controller('ClientEditController', 
                ['$scope', '$location', '$routeParams', 'Client', 'toaster', 
                    function($scope, $location, $routeParams, Client, toaster){        
                Client.get({id: $routeParams.id}, function(data){
                    $scope.client = data;
                    if (data.error) {
                        toaster.pop(data.type, "Mensagem", data.message);
                    }
                });
                $scope.edit = function(){
                    if($scope.frmEdit.$valid){
                        Client.update({id: $scope.client.id}, $scope.client, function(data){
                                toaster.pop(data.type, "Mensagem", data.message);
                                $location.path('/clients');   
                        });   
                    }
                };
        }]);