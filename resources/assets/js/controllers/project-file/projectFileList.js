angular.module('app.controllers')
        .controller('ProjectFileListController', ['$scope', '$routeParams', 'ProjectFile', function($scope, $routeParams, ProjectFile){
                $scope.error ={
                    message: '',
                    error: false
                };
                $scope.project_id = $routeParams.id;
                $scope.projectFiles = ProjectFile.query({id: $routeParams.id},
                    function(){
                        console.log('Carregado');
                    },function(data){
                        $scope.error.error = true;
                        $scope.error.message = data.data.error_description;
                    }
                );
        }]);