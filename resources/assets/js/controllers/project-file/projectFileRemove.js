angular.module('app.controllers')
        .controller('ProjectFileRemoveController', 
            ['$scope', '$location', '$routeParams','ProjectFile', 'toaster', function($scope, $location, $routeParams, ProjectFile, toaster){
                $scope.projectFile = ProjectFile.get({id: $routeParams.id, idFile: $routeParams.idFile});
                
                $scope.remove = function(){
                  $scope.projectFile.$delete({
                      id:$routeParams.id, idFile:$scope.projectFile.id
                  }).then(function(data){
                       toaster.pop(data.type, "Mensagem", data.message);
                      $location.path('/project/'+$routeParams.id +'/files');
                  });  
                };
        }]);

