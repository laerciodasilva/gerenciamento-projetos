angular.module('app.controllers')
        .controller('LoginModalController', ['$rootScope', '$scope', '$location',
                    '$cookies', '$uibModalInstance', 'authService', 'User', 'OAuth', 'OAuthToken', 'httpBuffer',
                    function($rootScope, $scope,  $location, $cookies, 
                            $uibModalInstance, authService, User, OAuth, OAuthToken, httpBuffer){
                $scope.user = {
                    username: '',
                    password: ''
                };
                $scope.error = {
                    message: '',
                    error: false
                };
                $scope.$on('event:auth-loginConfirmed', function(data, configUpdater){
                    $rootScope.loginModalOpened = false;
                    $uibModalInstance.close();
                });
                
                $scope.$on('$routeChangeStart', function() {
                    $rootScope.loginModalOpened = false;
                   $uibModalInstance.dismiss('cancel'); 
                  
                });
                
                $rootScope.$on('event:auth-loginCancelled', function() {
                    OAuthToken.removeToken();
                });
                $scope.login = function(){
                    if($scope.frmLogin.$valid){
                        OAuth.getAccessToken($scope.user).then(function(){
                            User.authenticated({},{},function(data){
                                $cookies.putObject('user', data);
                                //Reenvia requisão novamente
                                authService.loginConfirmed();
                            });
                        },function(data){
                            $scope.error.error= true;
                            $scope.error.message = data.data.error_description;
                        });
                    }
                };
                $scope.cancel = function(){
                  authService.loginCancelled();
                  $location.path('login');
                };
        }]);

