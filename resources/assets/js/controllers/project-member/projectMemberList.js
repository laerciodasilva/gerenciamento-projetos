angular.module('app.controllers')
        .controller('ProjectMemberListController', ['$scope', 'ProjectMember', 'User', 
                    '$routeParams', '$location', 'toaster',
                    function($scope, ProjectMember, User, $routeParams, $location, toaster){
                        $scope.projectMember = new ProjectMember();
                        
                        $scope.save = function(){
                            if($scope.form.$valid){
                                $scope.projectMember.$save({id: $routeParams.id}).then(function(data){
                                        $scope.projectMember = new ProjectMember();
                                        $scope.userSelected ='';
                                        $scope.loadMember();
                                        toaster.pop(data.type, data.title, data.message);
                                    },function(data){
                                        toaster.pop(data.type, data.title, data.message);
                                });
                            }
                        };
                        $scope.loadMember = function() {
                            $scope.projectMembers = ProjectMember.query({
                                id: $routeParams.id,
                                orderBy: 'id',
                                sortBy: 'desc'
                            
                            });
                        };
                        $scope.formatName = function(model){
                            if(model){
                                return model.name;
                            }
                            return '';
                        };

                        $scope.getUsers = function(name){
                            return User.query({
                                search: name,
                                searchFields: 'name:like'
                            }).$promise;
                        };

                        $scope.selectUser = function(item){
                            $scope.projectMember.member_id = item.id;
                        };
                        
                        $scope.loadMember();
        }]);