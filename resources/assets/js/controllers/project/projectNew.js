angular.module('app.controllers')
        .controller('ProjectNewController', ['$scope', 'Project', 'Client', 'appConfig', '$cookies', '$location', 'toaster', 
                    function($scope, Project, Client, appConfig, $cookies, $location, toaster){
                        $scope.project = new Project();
                        $scope.status = appConfig.project.status;
                        
                        $scope.save = function(){
                            if($scope.frmNew.$valid){
                                $scope.project.owner_id = $cookies.getObject('user').id;
                                $scope.project.client_id = $scope.project.clientId;
                                $scope.project.data_finalizacao = $scope.project.dataFinalizacao;
                                $scope.project.nome = $scope.project.projeto;
                                $scope.project.$save().then(function(data){
                                    toaster.pop(data.type, "Mensagem", data.message);
                                    $location.path('/projects');
                                },function(data){
                                   toaster.pop(data.type, "Mensagem", data.message);
                                   $location.path('/projects');
                               });
                            }
                        };
                        
                        $scope.formatName = function(model){
                            if(model){
                                return model.nome;
                            }
                            return '';
                        };

                        $scope.getClients = function(nome){
                            return Client.query({
                                search: nome,
                                searchFields: 'nome:like'
                            }).$promise;
                        };

                        $scope.selectClient = function(item){
                            $scope.project.clientId = item.id;
                        };
                        
                        $scope.dataFinalizacao = {
                            status: {
                                opened: false
                            }
                        };
                        
                        $scope.open = function(){
                            $scope.dataFinalizacao.status.opened = true;
                        };
        }]);
