angular.module('app.controllers')
        .controller('ProjectRemoveController', 
            ['$scope', '$location', '$routeParams','Project', 'toaster',
                function($scope, $location, $routeParams, Project, toaster){
                Project.get({id: $routeParams.id}, function(data){
                    $scope.project = data;
                    if (data.error) {
                        toaster.pop(data.type, "Mensagem", data.message);
                    }
                });
                $scope.remove = function(){
                  $scope.project.$delete({id:$scope.project.id}).then(function(data){
                      toaster.pop(data.type, "Mensagem", data.message);
                      $location.path('/projects');
                  });  
                };
        }]);

