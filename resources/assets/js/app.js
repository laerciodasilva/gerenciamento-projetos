var app = angular.module('app', ['ngRoute', 'angular-oauth2', 'toaster', 
 'ngAnimate', 'app.controllers','app.filters', 'app.directives', 'app.services',
 'ui.bootstrap.typeahead', 'ui.bootstrap.tpls', 'ui.bootstrap.datepicker', 
 'ngFileUpload', 'mgcrea.ngStrap.navbar', 'ui.bootstrap.dropdown', 
 'http-auth-interceptor', 'ui.bootstrap.modal'
 ]);

//Carrega controllers e dependência dos controllers 
angular.module('app.controllers', ['ngMessages','angular-oauth2','ngCookies', 'toaster']);
//Carrega Filters
angular.module('app.filters', []);
//Carrega Directives
angular.module('app.directives', []);
//Carrega Serviços e sua dependência
angular.module('app.services', ['ngResource']);

//Provider
//Nome do provider é acrescentado automáticamente ao criar o nome (appConfig == appConfigProvider)
app.provider('appConfig', ['$httpParamSerializerProvider', function($httpParamSerializerProvider){
    //Configurção do provedor
    var config = {
        baseUrl: 'http://gerenciamento-projetos.local',
        //Implementação de status
        project:{
            status:[
                {value: 1, label: 'Não inciado'},
                {value: 2, label: 'Inciado'},
                {value: 3, label: 'Concluido'}
            ]
        },
        projectTask:{
            status:[
                {value: 1, label: 'Imcompleta'},
                {value: 2, label: 'Completa'}
            ]
        },
        urls: {
            projectFile: '/project/{{id}}/file/{{idFile}}'
        },
        utils :{
            transformRequest: function(data){
                if(angular.isObject(data)){
                    return $httpParamSerializerProvider.$get()(data);
                }
                return data;
            },
            transformResponse: function(data,headers){
                //Para evitar que tranformer considere toda à resposta de json, usei headers para comparar
                var headerGetter = headers();
                if(headerGetter['content-type'] == 'application/json' ||
                   headerGetter['content-type'] == 'text/json'){
                     var dataJson = JSON.parse(data);
                    //Se a variavel tiver propriedade data
                    if(dataJson.hasOwnProperty('data')){

                       dataJson = dataJson.data;
                    }
                    return dataJson;
                }
              return data;
           }    
        }
    };
    
    //Retorna configuração do proverdor e serviços
    return {
        //Provedor
        config: config,
        //Serviço
        $get: function(){
            return config;
        }
    };
    
}]);

app.config(['$routeProvider', '$httpProvider', 'OAuthProvider', 'OAuthTokenProvider', 'appConfigProvider',
    function($routeProvider, $httProvider, OAuthProvider, OAuthTokenProvider, appConfigProvider){
    
    $httProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    
    
    $httProvider.defaults.transformRequest = appConfigProvider.config.utils.transformRequest;
    $httProvider.defaults.transformResponse = appConfigProvider.config.utils.transformResponse;
    
    //Registrando o service interceptor fix(relacionando à correção que estamos fazendo da mensagem access_denied)
    $httProvider.interceptors.splice(0, 1);
    $httProvider.interceptors.splice(0, 1);
    $httProvider.interceptors.push('OauthFixInterceptor');
    
    $routeProvider
        //Login
        .when('/login',{
            templateUrl: 'build/views/login.html',
            controller: 'LoginController'
        })
        .when('/logout', {
            resolve: {
                logout: ['$location', 'OAuthToken', function($location, OAuthToken) {
                    OAuthToken.removeToken();
                    $location.path('login');    
                }]
            }
        })
        //Home
        .when('/home', {
            templateUrl: 'build/views/home.html',
            controller: 'HomeController'
        })
        //Clientes
        .when('/clients', {
            templateUrl: 'build/views/client/list.html',
            controller: 'ClientListController'
        })
        .when('/clients/new', {
            templateUrl: 'build/views/client/new.html',
            controller: 'ClientNewController'
        })
        .when('/clients/edit/:id', {
            templateUrl: 'build/views/client/edit.html',
            controller: 'ClientEditController'
        })
        .when('/clients/remove/:id', {
            templateUrl: 'build/views/client/remove.html',
            controller: 'ClientRemoveController'
        })
        //Nota de projetos
        .when('/project/:id/notes', {
            templateUrl: 'build/views/project-note/list.html',
            controller:  'ProjectNoteListController'
        })
        .when('/project/:id/notes/show/:idNote', {
            templateUrl: 'build/views/project-note/show.html',
            controller:  'ProjectNoteShowController'
        })
        .when('/project/:id/notes/new', {
            templateUrl: 'build/views/project-note/new.html',
            controller:  'ProjectNoteNewController'
        })
        .when('/project/:id/notes/edit/:idNote', {
            templateUrl: 'build/views/project-note/edit.html',
            controller:  'ProjectNoteEditController'
        })
        .when('/project/:id/notes/remove/:idNote', {
            templateUrl: 'build/views/project-note/remove.html',
            controller:  'ProjectNoteRemoveController'
        })
         //Projetos
        .when('/projects', {
            templateUrl: 'build/views/project/list.html',
            controller:  'ProjectListController'
        })
        .when('/project/new', {
            templateUrl: 'build/views/project/new.html',
            controller:  'ProjectNewController'
        })
        .when('/project/edit/:id', {
            templateUrl: 'build/views/project/edit.html',
            controller:  'ProjectEditController'
        })
        .when('/project/remove/:id', {
            templateUrl: 'build/views/project/remove.html',
            controller:  'ProjectRemoveController'
        })
        //Project file
        .when('/project/:id/files', {
            templateUrl: 'build/views/project-file/list.html',
            controller:  'ProjectFileListController'
        })
        .when('/project/:id/file/new', {
            templateUrl: 'build/views/project-file/new.html',
            controller:  'ProjectFileNewController'
        })
        .when('/project/:id/file/edit/:idFile', {
            templateUrl: 'build/views/project-file/edit.html',
            controller:  'ProjectFileEditController'
        })
        .when('/project/:id/file/remove/:idFile', {
            templateUrl: 'build/views/project-file/remove.html',
            controller:  'ProjectFileRemoveController'
        })
        //Project task
        .when('/project/:id/task', {
            templateUrl: 'build/views/project-task/list.html',
            controller:  'ProjectTaskListController'
        })
        .when('/project/:id/task/new', {
            templateUrl: 'build/views/project-task/new.html',
            controller:  'ProjectTaskNewController'
        })
        .when('/project/:id/task/edit/:taskId', {
            templateUrl: 'build/views/project-task/edit.html',
            controller:  'ProjectTaskEditController'
        })
        .when('/project/:id/task/remove/:taskId', {
            templateUrl: 'build/views/project-task/remove.html',
            controller:  'ProjectTaskRemoveController'
        })
        //Project member
        .when('/project/:id/member', {
            templateUrl: 'build/views/project-member/list.html',
            controller:  'ProjectMemberListController'
        })
        .when('/project/:id/member/remove/:memberId', {
            templateUrl: 'build/views/project-member/remove.html',
            controller:  'ProjectMemberRemoveController'
        });
        
     //Provedor de autenticação   
    OAuthProvider.configure({
      baseUrl: appConfigProvider.config.baseUrl,//A url puxado do provider appConfig acima
      clientId: 'app1',
      clientSecret: 'secret', // optional
      grantPath: 'oauth/access_token',
    });
    //Usando autenticação não segura
    OAuthTokenProvider.configure({
        name: 'token',
        options: {
            secure: false
        }
    });
}]);

//Executado após  ativar angular
app.run(['$rootScope', '$location', '$http', '$uibModal', 'httpBuffer', 'OAuth', 
        function($rootScope, $location, $http, $uibModal, httpBuffer, OAuth) {
    
    //Evento que verifica mudança de rota. Next (próximo rot a) e current(rota atual)
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (next.$$route.originalPath != '/login') {
            if (!OAuth.isAuthenticated()) {
                $location.path('login');
            }
        }
    });
    
    $rootScope.$on('oauth:error', function(event, data) {
      // Ignore `invalid_grant` error - should be catched on `LoginController`.
      if ('invalid_grant' === data.rejection.data.error) {
        return;
      }

      // Refresh token when a `invalid_token` error occurs.
      if ('access_denied' === data.rejection.data.error) {
          
          httpBuffer.append(data.rejection.config, data.deffered);
          if (!$rootScope.loginModalOpened) {
              //Chamada de modal
              var modalInstance = $uibModal.open({
                  templateUrl: 'build/views/templates/loginModal.html',
                  controller: 'LoginModalController'
              });
              $rootScope.loginModalOpened = true;
              return;
          }
          
      }

      // Redirect to `/login` with the `error_reason`.
      return $location.path('login');
    });
}]);
