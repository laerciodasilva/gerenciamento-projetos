angular.module('app.services').
        service('OauthFixInterceptor', 
            ['$q', '$rootScope', 'OAuthToken', function ($q, $rootScope, OAuthToken) {
                return {
                    request: function request(config) {
                        config.headers = config.headers || {};
                        if (OAuthToken.getAuthorizationHeader()) {
                            config.headers.Authorization = OAuthToken.getAuthorizationHeader();
                        }
                        return config;
                    },
                    responseError: function responseError(rejection) {
                        //Objeto para protelar resposta e erro
                        var deffered = $q.defer();
                        if (400 === rejection.status && rejection.data && ("invalid_request" === rejection.data.error || "invalid_grant" === rejection.data.error)) {
                            OAuthToken.removeToken();
                            //$rootScope.$emit("oauth:error", rejection); Substituida por chamada abaixo para para passar o deffred e rejection junto
                            $rootScope.$emit("oauth:error", {rejection: rejection, deffered: deffered});
                        }
                        if (401 === rejection.status && rejection.data && "access_denied" === rejection.data.error 
                            || rejection.headers("www-authenticate") && 0 === rejection.headers("www-authenticate").indexOf("Bearer")) {
                                 //$rootScope.$emit("oauth:error", rejection); Substituida por chamada abaixo para para passar o deffred e rejection junto
                                $rootScope.$emit("oauth:error", {rejection: rejection, deffered: deffered});
                                return deffered.promise;
                        }
                        return $q.reject(rejection);
                        
                        //Retorn promessa que pode ser de erro ou sucesso
                        //return deffered.promise;
                    }
                };
        }]);

