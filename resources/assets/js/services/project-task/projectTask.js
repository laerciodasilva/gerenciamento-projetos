angular.module('app.services')
        .service('ProjectTask', ['$resource', '$filter', 'appConfig', function($resource, $filter, appConfig){
                function transformData(data){
                     if(angular.isObject(data) && (data.hasOwnProperty('due_date') || data.hasOwnProperty('start_date'))){
                         //copia objeto
                        var transform = angular.copy(data);
                        //formata data par aincluir no banco
                        transform.due_date = $filter('date')(data.due_date, 'yyyy-MM-dd');
                        transform.start_date = $filter('date')(data.start_date, 'yyyy-MM-dd');
                        return appConfig.utils.transformRequest(transform);
                    }
                    return data;
                };
                return $resource(appConfig.baseUrl +'/project/:id/task/:taskId', {id: '@id', taskId: '@taskId'},
                    {
                        save: {
                            method: 'POST',
                        },
                        get: {
                            method: 'GET',
                            transformResponse: function(data, headers){
                                var transform = appConfig.utils.transformResponse(data, headers);
                                 if(angular.isObject(transform) && (transform.hasOwnProperty('due_date') || transform.hasOwnProperty('start_date'))){
                                     //Formata data para exibir no formulário
                                    var arrayDueDate = transform.due_date.split('-'),
                                        monthDue = parseInt(arrayDueDate[1]) -1;
                                    transform.due_date = new Date(arrayDueDate[0], monthDue, arrayDueDate[2]);
                                    
                                    var arrayStartDate = transform.start_date.split('-'),
                                        monthStart = parseInt(arrayStartDate[1]) -1;
                                    transform.start_date = new Date(arrayStartDate[0], monthStart, arrayStartDate[2]);
                                }
                                return transform;
                            }
                        },
                        update: {
                            method: 'PUT',
                            transformRequest: transformData
                        }                   
                });
        }]);