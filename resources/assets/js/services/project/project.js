angular.module('app.services')
        .service('Project', ['$resource', '$filter', 'appConfig', function($resource, $filter, appConfig){
                
                function transformData(data){
                     if(angular.isObject(data) && data.hasOwnProperty('dataFinalizacao')){
                         //copia objeto
                        var transform = angular.copy(data);
                        //formata data par aincluir no banco
                        transform.dataFinalizacao = $filter('date')(data.dataFinalizacao, 'yyyy-MM-dd');
                        return appConfig.utils.transformRequest(transform);
                    }
                    return data;
                };
                
                return $resource(appConfig.baseUrl +'/project/:id',{id: '@id'}, {
                        save: {
                            method: 'POST',
                            transformRequest: transformData
                        },
                        get: {
                            method: 'GET',
                            transformResponse: function(data, headers){
                                var transform = appConfig.utils.transformResponse(data, headers);
                                 if(angular.isObject(transform) && transform.hasOwnProperty('dataFinalizacao')){
                                     //Formata data para exibir no formulário
                                    var arrayDate = transform.dataFinalizacao.split('-'),
                                        month = parseInt(arrayDate[1]) -1;
                                    transform.dataFinalizacao = new Date(arrayDate[0], month, arrayDate[2]);
                                }
                                return transform;
                            }
                        },
                        update: {
                            method: 'PUT',
                            transformRequest: transformData
                        }
                });
        }]);