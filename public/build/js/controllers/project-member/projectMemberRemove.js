angular.module('app.controllers')
        .controller('ProjectMemberRemoveController', 
            ['$scope', '$location', '$routeParams','ProjectMember', 'toaster',
                function($scope, $location, $routeParams, ProjectMember, toaster){
                    ProjectMember.get({id: $routeParams.id, memberId: $routeParams.memberId}, function(data){
                        if (data.error) {
                            toaster.pop(data.type, data.title, data.message);
                        }
                        $scope.projectMember =  data;
                    });
                    $scope.remove = function(){
                      $scope.projectMember.$delete({
                          id:$routeParams.id, memberId:$scope.projectMember.id
                      }).then(function(data){
                           toaster.pop(data.type, data.title, data.message);
                          $location.path('/project/'+$routeParams.id +'/member');
                      });  
                    };
        }]);

