angular.module('app.controllers')
        .controller('ProjectFileEditController', 
                ['$scope', '$location', '$routeParams', 'ProjectFile', 'toaster', function($scope, $location, $routeParams, ProjectFile, toaster){        
                $scope.projectFile = ProjectFile.get({id: $routeParams.id, idFile: $routeParams.idFile});
               
                $scope.edit = function(){
                    if($scope.frmEdit.$valid){
                        $scope.projectFile.project_id = $scope.projectFile.projectId;
                        ProjectFile.update(
                            {id:$routeParams.id, idFile:$scope.projectFile.id},
                            $scope.projectFile,
                            function(data){
                                toaster.pop(data.type, "Mensagem", data.message);
                                $location.path('/project/'+$routeParams.id +'/files');
                            },function(data){
                                toaster.pop(data.type, "Mensagem", data.message);
                            }
                        );
                            
                    }
                };
        }]);