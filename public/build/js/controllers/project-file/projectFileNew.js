angular.module('app.controllers')
        .controller('ProjectFileNewController', 
                        ['$scope', '$routeParams', '$location', 'appConfig', 
                            'Url', 'Upload', 'toaster',
                            function ($scope, $routeParams, $location, appConfig,
                            Url, Upload, toaster) 
            {
                $scope.save = function () {
                    if ($scope.frmNew.$valid, $scope.projectFile.file) {

                        var url = appConfig.baseUrl + 
                                Url.getUrlFromUrlSymbol(appConfig.urls.projectFile, 
                                {
                                    id: $routeParams.id,
                                    idFile: ''
                                });
                        Upload.upload({
                            url: url,
                            fields: {
                                nome: $scope.projectFile.nome,
                                descricao: $scope.projectFile.descricao,
                                projectId : $routeParams.id
                            },
                            file: $scope.projectFile.file 
                        }).then(function (data) {
                             toaster.pop(data.type, "Mensagem", data.message);
                             $location.path('/project/' + $routeParams.id + '/files');
                        }, function (data) {
                            toaster.pop(data.type, "Mensagem", data.message);
                        });
                    }
                };
            }]);
