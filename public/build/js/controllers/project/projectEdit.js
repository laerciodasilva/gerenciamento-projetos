angular.module('app.controllers')
        .controller('ProjectEditController', 
                ['$scope', '$location', '$routeParams', 'Project', 'Client', 'appConfig', '$cookies', 'toaster', 
                    function($scope, $location, $routeParams, Project, Client, appConfig, $cookies, toaster){        
                        Project.get({id:$routeParams.id}, function(data){
                            if (data.error) {
                                toaster.pop(data.type, "Mensagem", data.message);
                            }
                            $scope.project = data;
                            $scope.clientSelected = $scope.project.client;
                        });
                        
                        $scope.status = appConfig.project.status;
                        $scope.edit = function(){
                            $scope.project.owner_id = $cookies.getObject('user').id;
                            $scope.project.client_id = $scope.project.clientId;
                            $scope.project.data_finalizacao = $scope.project.dataFinalizacao;
                            $scope.project.nome = $scope.project.projeto;
                            Project.update({id: $scope.project.id}, $scope.project, 
                                function(data){
                                    toaster.pop(data.type, "Mensagem", data.message);
                                   $location.path('/projects'); 
                                },function(data){
                                    toaster.pop(data.type, "Mensagem", data.message);
                                }
                            );
                        };
                        
                        $scope.formatName = function(model){
                            if(model){
                                return model.nome;
                            }
                            return '';
                        };
                        
                        $scope.getClients = function(nome){
                            return Client.query({
                                search: nome,
                                searchFields: 'nome:like'
                            }).$promise;
                        };
                        
                        $scope.selectClient = function(item){
                            $scope.project.clientId = item.id;
                        };
                        
                         $scope.dataFinalizacao = {
                            status: {
                                opened: false
                            }
                        };
                        
                        $scope.open = function(){
                            $scope.dataFinalizacao.status.opened = true;
                        };
        }]);