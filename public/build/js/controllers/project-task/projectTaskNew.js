angular.module('app.controllers')
        .controller('ProjectTaskNewController', ['$scope', 'ProjectTask', 
            '$routeParams', '$location', 'toaster', 'appConfig',
                    function($scope, ProjectTask, $routeParams, $location, toaster, appConfig){
                $scope.projectTask = new ProjectTask();
                
                $scope.save = function(){
                    if($scope.form.$valid){
                        $scope.projectTask.status = appConfig.projectTask.status[0].value;
                        $scope.projectTask.$save({id: $routeParams.id}).then(function(data){
                                toaster.pop(data.type, data.title, data.message);
                                $location.path('/project/'+$routeParams.id +'/task');
                            },function(data){
                                toaster.pop(data.type, data.title, data.message);
                                $location.path('/project/'+$routeParams.id +'/task');
                        });
                    }
                };
        }]);
