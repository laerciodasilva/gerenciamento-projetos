angular.module('app.controllers')
        .controller('ProjectTaskRemoveController', 
            ['$scope', '$location', '$routeParams','ProjectTask', 'toaster',
                function($scope, $location, $routeParams, ProjectTask, toaster){
                    ProjectTask.get({id: $routeParams.id, taskId: $routeParams.taskId}, function(data){
                        if (data.error) {
                            toaster.pop(data.type, data.title, data.message);
                        }
                        $scope.projectTask =  data;
                    });
                    $scope.remove = function(){
                      $scope.projectTask.$delete({
                          id:$routeParams.id, taskId:$scope.projectTask.id
                      }).then(function(data){
                           toaster.pop(data.type, data.title, data.message);
                          $location.path('/project/'+$routeParams.id +'/task');
                      });  
                    };
        }]);

