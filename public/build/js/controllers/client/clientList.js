angular.module('app.controllers')
        .controller('ClientListController', ['$scope', 'Client', 'toaster',
            function($scope, Client, toaster){
                $scope.clients=Client.query(
                    function(){
                        
                    },function(data){
                        toaster.pop('error', "Mensagem", data.data.error_description);
                    }
                );
        }]);