angular.module('app.controllers')
        .controller('ClientNewController', ['$scope', 'Client', '$location', 'toaster', function($scope, Client, $location, toaster){
                $scope.client = new Client();
                
                $scope.save = function(){
                    if($scope.frmNew.$valid){
                        $scope.client.$save().then(function(data){
                             toaster.pop('success', "Mensagem", 'Cliente gravado com sucesso!');
                             $location.path('/clients');
                         },function(data){
                            toaster.pop(data.type, "Mensagem", data.message);
                            $location.path('/clients');
                        });
                    }
                };
        }]);
