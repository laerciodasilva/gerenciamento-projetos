angular.module('app.controllers')
        .controller('ProjectNoteEditController', 
                ['$scope', '$location', '$routeParams', 'ProjectNote', 'toaster', function($scope, $location, $routeParams, ProjectNote, toaster){     
                        $scope.projectNote = {};
                        ProjectNote.get({id:$routeParams.id,idNote: $routeParams.idNote}, function(data){
                            if (data.error) {
                                toaster.pop(data.type, "Mensagem", data.message);
                            }
                            $scope.projectNote = data;
                        });
               
                $scope.edit = function(){
                    if($scope.frmEdit.$valid){
                        ProjectNote.update(
                            {id:$routeParams.id, idNote:$scope.projectNote.id},
                            $scope.projectNote,
                            function(data){
                                toaster.pop(data.type, "Mensagem", data.message);
                                $location.path('/project/'+$routeParams.id +'/notes');
                            },function(data){
                                toaster.pop('error', "Erro", 'Ocorreu um erro');
                            }
                        );
                            
                    }
                };
        }]);