angular.module('app.directives')
        .directive('projectFileDownload', ['$timeout', 'appConfig', 'ProjectFile', 'toaster',
            function ($timeout, appConfig, ProjectFile, toaster) {
                return {
                    restrict: 'E',
                    templateUrl: appConfig.baseUrl + '/build/views/templates/projectFileDownload.html',
                    link: function(scope, element, attr) {
                        var anchor = element.children()[0];
                            scope.$on('salvar-arquivo', function(event, data){
                                $(anchor).removeClass('disabled');
                                $(anchor).text('Download');
                                $(anchor).attr({
                                   href: 'data:application-octet-stream;base64,' + data.file,
                                   download: data.nome
                                });
                                $timeout(function(){
                                    scope.downloadFile = function(){};
                                   $(anchor)[0].click(); 
                                });
                        });
                    },
                    controller: ['$scope', '$element', '$attrs', '$routeParams', 'toaster', function($scope, $element, $attrs, $routeParams, toaster) {
                        $scope.downloadFile = function(){
                            var anchor = $element.children()[0];
                            $(anchor).addClass('disabled');
                            $(anchor).text('Carregando...');
                            toaster.pop('', 'Download', 'Processando...');
                            ProjectFile.download({id: $routeParams.id, idFile: $attrs.idFile}, function (data){
                                $scope.$emit('salvar-arquivo', data);
                                toaster.pop(data.type, "Mensagem", data.message);
                            });
                        };
                    }]
                };
            }]);
