angular.module('app.services')
        .service('Url', ['$interpolate',function($interpolate) {
                return {
                    //Esse objeto substitui barra dos paramêtros vazio da url por uma barra somente
                    //Ex: url :'/project/:id/file/:idFile/' url vazio: '/project//file/'
                    getUrlFromUrlSymbol: function (url, params) {
                        //interpolate substitui um valor por outro. Ex: $interpolate({{teste}})({'teste' : "Pedro"}) Substitui teste por Pedro
                        var urlMod = $interpolate(url)(params);
                        return urlMod.replace(/\/\//g, '/')
                                .replace(/\/$/, '');
                    },
                    //Esse objeto rece a url e substitui por formato real da url do angular
                    //Ex: '/project/{{id}}/file/{{idFile}}'
                    getUrlResource: function (url) {
                        return url.replace(new RegExp('{{', 'g'), ':')
                                .replace(new RegExp('}}', 'g'), '');
                    }
                }
        }]);