<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\User;
use League\Fractal\TransformerAbstract;

/**
 * Description of ProjectTransformer
 *
 * @author laercio
 */
class MemberTransformer extends TransformerAbstract{
 
    public function transform(User $member){
        
        return [
            'membro_id' => $member->id,
            'membro_nome' => $member->name,
        ];
    }
    
}
