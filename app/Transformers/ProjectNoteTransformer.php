<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\ProjectNote;
use League\Fractal\TransformerAbstract;

/**
 * Description of ProjectNoteTransformers
 *
 * @author laercio
 */
class ProjectNoteTransformer extends TransformerAbstract {
   
    public function transform(ProjectNote $projectNote){
     
        return [
            'id' => $projectNote->id,
            'project_id' => $projectNote->project_id,
            'titulo' => $projectNote->titulo,
            'anotacao' => $projectNote->anotacao
        ];
    }
}
