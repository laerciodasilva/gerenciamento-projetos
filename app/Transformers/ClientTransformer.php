<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\Client;
use League\Fractal\TransformerAbstract;

/**
 * Description of ProjectTransformer
 *
 * @author laercio
 */
class ClientTransformer extends TransformerAbstract{

    public function transform(Client $client){
        
        return [
            'id' => $client->id,
            'nome' => $client->nome,
            'responsavel' => $client->responsavel,
            'email' => $client->email,
            'telefone' => $client->telefone,
            'endereco' => $client->endereco,
            'obs' => $client->obs
        ];
    }
}
