<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\Project;
use League\Fractal\TransformerAbstract;

/**
 * Description of ProjectTransformer
 *
 * @author laercio
 */
class ProjectTransformer extends TransformerAbstract{
 
    protected $defaultIncludes = ['members'];


    public function transform(Project $project){
        
        return [
            'id' => $project->id,
            'projeto' => $project->nome,
            'clientId' => $project->client_id,
            'client' => $project->client,
            'ownerId' => $project->owner_id,
            'owner' => $project->user,
            'descricao' => $project->descricao,
            'progresso' => (int)$project->progresso,
            'status' => $project->status,
            'dataFinalizacao' => $project->data_finalizacao,
            'isOwner' => $project->owner_id != \Authorizer::getResourceOwnerId(),
        ];
    }
    
    public function includeMembers(Project $project){
     return $this->collection($project->members, new MemberTransformer());
    }
    
    public function includeClient(Project $project){
        return $this->collection($project->client, new ClientTransformer());
    }
}
