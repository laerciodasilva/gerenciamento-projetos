<?php

namespace CodeProject\Transformers;

use League\Fractal\TransformerAbstract;
use CodeProject\Entities\OauthCliente;

/**
 * Class OauthClienteTransformer
 * @package namespace CodeProject\Transformers;
 */
class OauthClienteTransformer extends TransformerAbstract
{

    /**
     * Transform the \OauthCliente entity
     * @param \OauthCliente $model
     *
     * @return array
     */
    public function transform(OauthCliente $model)
    {
        return [
            'id'         => (int) $model->id,
        ];
    }
}
