<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\ProjectFile;
use League\Fractal\TransformerAbstract;

/**
 * Description of ProjectNoteTransformers
 *
 * @author laercio
 */
class ProjectFileTransformer extends TransformerAbstract {
   
    public function transform(ProjectFile $projectFile){
     
        return [
            'id' => $projectFile->id,
            'projectId' => $projectFile->project_id,
            'nome' => $projectFile->nome,
            'extensao' => $projectFile->extensao,
            'descricao' => $projectFile->descricao
        ];
    }
}
