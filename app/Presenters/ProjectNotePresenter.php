<?php
namespace CodeProject\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use CodeProject\Transformers\ProjectNoteTransformer;
/**
 * Description of ProjectPresenter
 *
 * @author laercio
 */
class ProjectNotePresenter extends FractalPresenter{
    
    public function getTransformer() {
       return new ProjectNoteTransformer();
    }
}
