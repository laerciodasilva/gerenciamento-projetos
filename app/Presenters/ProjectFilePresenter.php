<?php
namespace CodeProject\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use CodeProject\Transformers\ProjectFileTransformer;
/**
 * Description of ProjectPresenter
 *
 * @author laercio
 */
class ProjectFilePresenter extends FractalPresenter{
    
    public function getTransformer() {
       return new ProjectFileTransformer();
    }
}
