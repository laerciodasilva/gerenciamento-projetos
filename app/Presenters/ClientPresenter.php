<?php
namespace CodeProject\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use CodeProject\Transformers\ClientTransformer;
/**
 * Description of ProjectPresenter
 *
 * @author laercio
 */
class ClientPresenter extends FractalPresenter{
    
    public function getTransformer() {
       return new ClientTransformer();
    }
}
