<?php

namespace CodeProject\Presenters;

use CodeProject\Transformers\OauthClienteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OauthClientePresenter
 *
 * @package namespace CodeProject\Presenters;
 */
class OauthClientePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OauthClienteTransformer();
    }
}
