<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('app');
});

/**
 * routa com função anônima
 * retorna json
Route::get('client',function(){
	return \CodeProject\Client::all();
});
*/

Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});
Route::group(['middleware' => 'oauth'], function(){
    Route::resource('client','ClientController',['except' => ['create', 'edit']]);
    /*
     * Uso de meddleware para verificação do project, mas também pode ser feito no controller 
     * Route::group(['middleware' => 'check-owner-project'], function(){
        Route::resource('project','ProjectController',['except' => ['create', 'edit']]);
    });*/
    Route::resource('project','ProjectController',['except' => ['create', 'edit']]);
    
    //project.member é mesma coisa com /project/{project}/member{memember}
    Route::resource('project,member','ProjectController',['except' => ['create', 'edit', 'update']]);
    
    Route::group(['middleware' => 'check-project-permission','prefix' => 'project'], function(){
        
        Route::get('{id}/note','ProjectNoteController@index');
        Route::post('{id}/note','ProjectNoteController@store');
        Route::put('{id}/note/{noteId}','ProjectNoteController@update');
        Route::get('{id}/note/{noteId}','ProjectNoteController@show');
        Route::delete('{id}/note/{noteId}','ProjectNoteController@destroy');
        
        Route::get('{id}/member','ProjectMemberController@index');
        Route::post('{id}/member','ProjectMemberController@store');
        Route::put('{id}/member/{memberId}','ProjectMemberController@update');
        Route::get('{id}/member/{memberId}','ProjectMemberController@show');
        Route::delete('{id}/member/{memberId}','ProjectMemberController@destroy');
        
        
        Route::get('{id}/task','ProjectTaskController@index');
        Route::put('{id}/task/{taskId}','ProjectTaskController@update');
        Route::get('{id}/task/{taskId}','ProjectTaskController@show');
        Route::delete('{id}/task/{taskId}','ProjectTaskController@destroy');
        Route::post('{id}/task','ProjectTaskController@store');
        
        
        Route::get('{id}/file','ProjectFileController@index');
        Route::post('{id}/file','ProjectFileController@store');
        Route::get('{id}/file/{fileId}','ProjectFileController@show');
        Route::put('{id}/file/{fileId}','ProjectFileController@update');
        Route::get('{id}/file/{fileId}/download','ProjectFileController@showFile');
        Route::delete('{id}/file/{fileId}','ProjectFileController@destroy');
        Route::post('{id}/file','ProjectFileController@store');
        
    });
    
    Route::get('user/authenticated', 'UserController@authenticated');
    Route::resource('user', 'UserController', ['except' => ['create', 'edit']]);
});
