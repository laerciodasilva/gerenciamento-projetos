<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Http\Controllers\Controller;
use CodeProject\Repositories\ClientRespository;
use CodeProject\Services\ClientService;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClientController extends Controller
{
    /**
     * @var ClientRespository
     */
    protected $repository;
    
    /**
     *
     * @var ClientService 
     */
    protected $service;
    
    /**
     * 
     * @param ClientRespository $repository
     * @param ClientService $service
     */
    public function __construct(ClientRespository $repository, ClientService $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            return $this->service->create($request->all());
        } catch (QueryException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Erro no sql ao gravar dados.'
                ];
        } catch (ModelNotFoundException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Cliente não encontrado.'
                ];
        } catch (\Exception $e) {
                return [
                    'error' => true,
                    'type' => 'error', 
                    'message' => 'Ocorreu um erro ao gravar o projeto.'
                ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try{
            return $this->repository->find($id);
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Cliente não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao procurar cliente.'
            ];
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = $this->service->update($request->all(), $id);
            return [
                'error' => false, 
                'type' => 'success', 
                $data,
                'message' => 'Dados alterados com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao alterar os dados'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Cliente não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao excluir o cliente.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->repository->delete($id);
            return [
                'error' => false, 
                'type' => 'success', 
                'message' => 'Dado excluido com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Cliente não pode ser excluido, pois existe um projeto ou mais projetos vinculado a ele.'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Cliente não encontrado'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao excluir o cliente.'
            ];
        }
    }
}
