<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Http\Controllers\Controller;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Services\ProjectService;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    protected $repository;
    
    /**
     *
     * @var ProjectService 
     */
    protected $service;
    
    /**
     * 
     * @param ProjectRepository $repository
     * @param ProjectService $service
     */
    public function __construct(ProjectRepository $repository, ProjectService $service) {
        $this->repository = $repository;
        $this->service = $service;
        $this->middleware('check-owner-project', ['except' => ['index', 'store', 'show']]);
        $this->middleware('check-project-permission', ['except' => ['index','store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //return $this->repository->findWhere(['owner_id' => \Authorizer::getResourceOwnerId()]);
        return $this->repository->findWithOwnerAndMenber(\Authorizer::getResourceOwnerId());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            $data = $this->service->create($request->all());
            return [
               'error' => false, 
               'type' => 'success', 
               $data,
               'message' => 'Dados gravados com sucesso!'
           ];
        } catch (QueryException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Erro no sql ao gravar dados.'
                ];
        } catch (\Exception $e) {
                return [
                    'error' => true,
                    'type' => 'error', 
                    'message' => 'Ocorreu um erro ao gravar o projeto.'
                ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try{
            if(!$this->service->checkProjectPermissions($id)){
                return [
                    'error' => true, 
                    'message' => 'Access Forbiden',
                    'type' => 'warning'
                ];
            }

            return $this->repository->find($id);
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Projeto não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao procurar projeto.'.$e->getMessage()
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try{
            if(!$this->service->checkProjectPermissions($id)){
                return [
                    'error' => true, 
                    'message' => 'Access Forbiden',
                    'type' => 'warning'
                ];
            }
            $data = $this->service->update($request->all(), $id);
            return [
                'error' => false, 
                'type' => 'success', 
                $data,
                'message' => 'Dados gravados com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao alterar os dados'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Projeto não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao editar o projeto.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       try{
            if(!$this->service->checkProjectPermissions($id)){
                return ['error' => 'Access Forbiden'];
            }
            $this->repository->delete($id);
            return [
                'error' => false, 
                'type' => 'success', 
                'message' => 'Projeto excluido com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Projeto não pode ser excluido, pois existe nota vinculado a ele.'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Projeto não encontrado'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao excluir o projeto.'
            ];
        }
    }       
}
