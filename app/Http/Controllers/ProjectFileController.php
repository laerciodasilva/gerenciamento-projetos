<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Http\Controllers\Controller;
use CodeProject\Repositories\ProjectFileRepository;
use CodeProject\Services\ProjectFileService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProjectFileController extends Controller
{
    /**
     * @var ProjectFileRepository
     */
    protected $repository;
    
    /**
     *
     * @var ProjectFileService 
     */
    protected $service;
    
    /**
     * 
     * @param ProjectFileRepository $repository
     * @param ProjectFileService $service
     */
    public function __construct(ProjectFileRepository $repository, ProjectFileService $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $data['file'] = $file;
            $data['extensao'] = $extension;
            $data['nome'] = $request->nome;
            $data['project_id'] = $request->projectId;
            $data['descricao'] = $request->descricao;
            $data = $this->service->create($data);
            return [
                'error' => false, 
                'type' => 'success', 
                $data,
                'message' => 'Dados gravados com sucesso!'
            ];
        } catch (QueryException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Erro no sql ao gravar dados.'
                ];
        } catch (ModelNotFoundException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Cliente não encontrado.'
                ];
        } catch (\Exception $e) {
                return [
                    'error' => true,
                    'type' => 'error', 
                    'message' => 'Ocorreu um erro ao gravar o projeto.'.$e->getMessage()
                ];
        }
    }
    /**
     * Retorna o byte do arquivo para o response do laravel ao passsar o caminho
     * @param type $id
     * @param type $fileId
     * @return type
     */
    public function showFile($id, $fileId)
    {
        try{
            if(!$this->service->checkProjectPermissions($fileId)){
                return ['error' => 'Access Forbiden'];
            }
            $filePath = $this->service->getFilePath($fileId);
            $faliContent = file_get_contents($filePath);
            $file64 = base64_encode($faliContent);
            return [
                'file' => $file64,
                'size' => filesize($filePath),
                'nome' => $this->service->getFileName($fileId),
                'error' => false,
                'type' => 'success', 
                'message' => 'Arquivo baixado com sucesso'
            ];
         } catch (QueryException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Erro no sql ao gravar dados.'
                ];
        } catch (ModelNotFoundException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Arquivo não encontrado.'
                ];
        } catch (\Exception $e) {
                return [
                    'error' => true,
                    'type' => 'error', 
                    'message' => 'Ocorreu baixar o arquivo.'
                ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id,$idFile)
    {
        try{
            if(!$this->service->checkProjectPermissions($idFile)){
                return ['error' => 'Access Forbiden'];
            }

            $result = $this->repository->findWhere(['project_id' => $id, 'id' => $idFile]);
            if($result['data'] && count($result['data']) == 1){
                $result = [
                  'data' => $result['data'][0]  
                ];
            }
            return $result;
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao alterar os dados'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Arquivo não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao exibir o arquivo.'
            ];
        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id, $fileId)
    {
        try{
            if(!$this->service->checkProjectPermissions($fileId)){
                return ['error' => 'Access Forbiden'];
            }
            $data = $this->service->update($request->all(), $fileId);
            return [
                'error' => false, 
                'type' => 'success', 
                $data,
                'message' => 'Dados gravado com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao alterar os dados'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Arquivo não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao editar o arquivo.'.$e->getMessage()
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, $fileId)
    {
        try{
            if(!$this->service->checkProjectPermissions($fileId)){
                return ['error' => 'Access Forbiden'];
            }

            $this->service->delete($fileId);
            return [
                'error' => false, 
                'type' => 'success', 
                'message' => 'Arquivo excluido com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Arquivo não pode ser excluido, pois existe nota vinculado a ele.'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Projeto não encontrado'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao excluir o arquivo.'
            ];
        }
    }        
}
