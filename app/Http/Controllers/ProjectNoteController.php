<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Http\Controllers\Controller;
use CodeProject\Repositories\ProjectNoteRepository;
use CodeProject\Services\ProjectNoteService;

class ProjectNoteController extends Controller
{
    /**
     * @var ProjectNoteRepository
     */
    protected $repository;
    
    /**
     *
     * @var ProjectNoteService 
     */
    protected $service;
    
    /**
     * 
     * @param ProjectNoteRepository $repository
     * @param ProjectNoteService $service
     */
    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
         try{
            $data = $this->service->create($request->all());
            return [
               'error' => false, 
               'type' => 'success',
               'message' => 'Dados gravados com sucesso!'
           ];
        } catch (QueryException $e) {
                return [
                    'error' => true,
                    'type' => 'warning', 
                    'message' => 'Erro no sql ao gravar dados.'
                ];
        } catch (\Exception $e) {
                return [
                    'error' => true,
                    'type' => 'error', 
                    'message' => 'Ocorreu um erro ao gravar nota de projeto.'
                ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id,$idNote)
    {
        try{
            $result = $this->repository->findWhere(['project_id' => $id, 'id' => $idNote]);
            if(isset($result['data']) && count($result['data']) == 1){
                $result =[
                    'data' => $result['data'][0]
                ];
            }
            return $result;
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Nota de projeto não encontrada.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro ao procurar nota deprojeto.'.$e->getMessage()
            ];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = $this->service->update($request->all(), $id);
            return [
                'error' => false, 
                'type' => 'success', 
                $data,
                'message' => 'Dados alterado com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao alterar os dados'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Nota de projeto não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro no sistema.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, $noteId)
    {
        try{
            
            $this->repository->delete($noteId);
            return [
                'error' => false, 
                'type' => 'success', 
                'message' => 'Dados excluidos com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql.'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Nota de projeto não encontrado'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro no sistema.'.$e->getTrace()
            ];
        }
    }
}
