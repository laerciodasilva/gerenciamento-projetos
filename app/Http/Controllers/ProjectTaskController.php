<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Http\Controllers\Controller;
use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Services\ProjectTaskService;
use CodeProject\Entities\Core;

class ProjectTaskController extends Controller
{
    /**
     *
     * @var ProjectTaskRepository
     */
    protected $repository;
    
    /**
     *
     * @var ProjectTaskService
     */
    protected $service;
    
    /**
     * 
     * @param ProjectTaskRepository $repository
     * @param ProjectTaskService $Service
     */
    public function __construct(ProjectTaskRepository $repository, ProjectTaskService $Service) 
    {
        $this->repository = $repository;
        $this->service    = $Service;
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index($id)
    {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        try{
            $data = $request->all();
            $data['project_id'] = $id;
            $data = $this->service->create($data);
            $mensagem = Core::messageValidate($data);
            if (!is_null($mensagem)) {
                return [
                    'error' => true, 
                    'type' => 'error', 
                    'title' => 'Campo(s) obrigatório(s)', 
                    'message' => $mensagem
                ];
            }
            return [
               'error' => false, 
               'type'  => 'success',
               'title' => 'Obrigado', 
               'date'  => $data,
               'message' => 'Dados gravados com sucesso!'
           ];
        } catch (QueryException $e){
            return [
                'error' => true,
                'type' => 'warning', 
                'title' => 'Erro Sql',
                'message' => 'Erro no sql.'
            ];
        } catch (\Exception $e) {
            return [
                'error'   => true, 
                'type'    => 'error',
                'title' => 'Erro técnico',
                'message' => 'Ocorreu um erro no sistema.'.$e->getMessage()
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $taskId)
    {
       try{
            $result = $this->repository->find($taskId);
            if(isset($result['data']) && count($result['data']) == 1){
                $result = [
                    'data' => $result['data'][0]
                ];
            }
             return $result;
            
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Model',
                'message' => 'Tarefa não encontrada.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error',
                'title' => 'Sistema',
                'message' => 'Ocorreu um erro no sistema.'
            ];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $taskId)
    {
        try{
            $data = $request->all();
            $data['project_id'] = $id;
            $data = $this->service->update($data, $taskId);
            $mensagem = Core::messageValidate($data);
            if (!is_null($mensagem)) {
                return [
                'error' => true, 
                'type' => 'error', 
                'title' => 'Campo(s) obrigatório(s)', 
                'message' => $mensagem
                ];
            }
            return [
                'error' => false, 
                'type' => 'success', 
                'title' => 'Obrigado',
                $data,
                'message' => 'Dados gravados com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Sql',
                'message' => 'Erro no sql'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Model',
                'message' => 'Tarefa não encontrada.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error',
                'title' => 'Sistema',
                'message' => 'Ocorreu um erro no sistema.'.$e->getMessage()
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $taskId)
    {
        try{
            
            $this->service->delete($taskId);
            return [
                'error' => false, 
                'type' => 'success',
                'title' => 'Obrigado',
                'message' => 'Tarefa excluida com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Sql',
                'message' => 'Erro no sql'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Model',
                'message' => 'Tarefa não encontrado'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error',
                'title' => 'Sistema',
                'message' => 'Ocorreu um erro no sistema.'
            ];
        }
    }
}
