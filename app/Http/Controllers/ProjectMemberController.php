<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Http\Controllers\Controller;
use CodeProject\Repositories\ProjectMemberRepository;
use CodeProject\Services\ProjectMemberService;
use Illuminate\Database\QueryException;
use CodeProject\Entities\Core;

class ProjectMemberController extends Controller
{
    /**
     *
     * @var ProjectMemberRepository
     */
    protected $repository;
    
    /**
     *
     * @var ProjectMemberService
     */
    protected $service;
    
    public function __construct(ProjectMemberRepository $repository, ProjectMemberService $Service) 
    {
        $this->repository = $repository;
        $this->service    = $Service;
        $this->middleware('check-owner-project', ['except' => ['index', 'show']]);
        $this->middleware('check-project-permission', ['except' => ['store','destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index($id)
    {
        return $this->repository->findWhere(['project_id' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        try{
            $data = $request->all();
            $data['project_id'] = $id;
            $data = $this->service->create($data);
            $mensagem = Core::messageValidate($data);
            if (!is_null($mensagem)) {
                return [
                'error' => true, 
                'type' => 'error', 
                'title' => 'Campo(s) obrigatório(s)', 
                'message' => $mensagem
                ];
            }
            return [
               'error' => false, 
               'type'  => 'success',
               'title' => 'Obrigado', 
               'date'  => $data,
               'message' => 'Dados gravados com sucesso!'
           ];
        } catch (QueryException $e){
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao gravar dados.'
            ];
        } catch (\Exception $e) {
            return [
                'error'   => true, 
                'type'    => 'error', 
                'message' => 'Ocorreu um erro ao gravar o membro.'
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$memberId)
    {
       try{
            $result = $this->repository->find($memberId);
            return $result;
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Membro de projeto não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro no sistema.'
            ];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = $this->service->update($request->all(), $id);
            return [
                'error' => false, 
                'type' => 'success', 
                $data,
                'message' => 'Dados alterado com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Erro no sql ao alterar os dados'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning', 
                'message' => 'Membro de projeto não encontrado.'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error', 
                'message' => 'Ocorreu um erro no sistema.'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $memberId)
    {
        try{
            
            $this->repository->delete($memberId);
            return [
                'error' => false, 
                'type' => 'success',
                'title' => 'Obrigado',
                'message' => 'Membro excluído com sucesso!'
            ];
        } catch (QueryException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Sql',
                'message' => 'Erro no sql'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'type' => 'warning',
                'title' => 'Model',
                'message' => 'Tarefa não encontrado'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'type' => 'error',
                'title' => 'Sistema',
                'message' => 'Ocorreu um erro no sistema.'
            ];
        }
    }
}
