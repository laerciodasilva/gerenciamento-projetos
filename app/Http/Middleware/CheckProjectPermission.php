<?php

namespace CodeProject\Http\Middleware;

use Closure;
use CodeProject\Services\ProjectService;

class CheckProjectPermission
{ 
    /**
     *
     * @var type ProjectService 
     */
    private $service;
    
    public function __construct(ProjectService $service) 
    {
        $this->service = $service;
    }

        /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $projectId = $request->route('id') ? $request->route('id') : $request->route('project');
        if (!$this->service->checkProjectPermissions($projectId)) {
            //return ['error' => 'You haven´t permission to acess project'];
            return [
                'error' => true,
                'type' => 'info', 
                'message' => 'Você não tem permissão para acessar o projeto'
            ];
        }
        return $next($request);
    }
}
