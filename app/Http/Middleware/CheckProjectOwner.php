<?php

namespace CodeProject\Http\Middleware;

use Closure;
use CodeProject\Services\ProjectService;

class CheckProjectOwner
{
    /**
     *
     * @var type 
     */
    protected $service;

    /**
     * 
     * @param ProjectRepository $repository
     */
    public function __construct(ProjectService $service) {
       
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         //Retoran id do usuário logado
        $userId = \Authorizer::getResourceOwnerId();
        
       $projectId = $request->route('id') ? $request->route('id') : $request->route('project');
       
        if(!$this->service->checkProjectOwner($projectId)){
            //return ['error' => 'Access forbidden'];
            return [
                'error' => true,
                'type' => 'info', 
                'message' => 'Você não tem permissão'
            ];
        }
        return $next($request);
    }
}
