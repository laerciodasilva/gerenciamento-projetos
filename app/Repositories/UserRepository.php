<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Description of UserRepository
 *
 * @author laercio
 */
interface UserRepository extends RepositoryInterface{
    
}
