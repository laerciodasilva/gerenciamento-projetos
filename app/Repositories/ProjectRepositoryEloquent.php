<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeProject\Entities\Project;
use CodeProject\Presenters\ProjectPresenter;

/**
 * Class ProjectRepositoryEloquent
 * @package namespace CodeProject\Repositories;
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria( app(RequestCriteria::class) );
    }
    /**
     * Verifica se o usuário tem permissão para alterar o projeto
     * @param type $projectId
     * @param type $userId
     * @return boolean
     */
    public function isOwner($projectId,$userId){
        if(count($this->skipPresenter()->findWhere(['id' => $projectId, 'owner_id' => $userId]))){
            return true;
        }
        
        return false;
    }
    /**
     * Verifica se um determinado usuário tem permissão para acessar o projeto
     * @param type $projectId
     * @param type $memberId
     * @return boolean
     */
    public function hasMamber($projectId,$memberId){
        $project = $this->skipPresenter()->find($projectId);
        
        foreach ($project->members as $member){
            if($member->id == $memberId){
                return true;
            }
        }
        
        return false;
    }
    
    public function findWithOwnerAndMenber($userId)
    {
        return $this->scopeQuery(function($query) use($userId){
           return $query->select('projects.*')
                   ->leftJoin('project_members', 'projects.id', '=', 'project_members.project_id')
                  ->where('project_members.member_id', '=', $userId)
                  ->union($this->model->query()->getQuery()->where('owner_id', '=', $userId));
        })->all();
    }

    public function presenter() {
        return ProjectPresenter::class;
    }
}