<?php

/**
 * Interface que extende interface de repository para usar métodos como create,find,update...
 */
namespace CodeProject\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;


/**
 *
 * @author laercio
 */
interface ClientRespository extends RepositoryInterface{
    
}
