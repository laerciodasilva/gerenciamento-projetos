<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use CodeProject\Repositories\UserRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeProject\Entities\User;
/**
 * Description of UserRepositoryEloquent
 *
 * @author laercio
 */
class UserRepositoryEloquent  extends BaseRepository implements UserRepository{
    
    protected $fieldSearchable =[
        'name'
    ];
    
    public function model() {
        return User::class; 
    }
    
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
