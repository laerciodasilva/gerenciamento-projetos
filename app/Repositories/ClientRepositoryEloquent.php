<?php
/**
 * Classe concreta do repositore que extende repository base, e implementa interface do Client repository
 *
 */
namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeProject\Entities\Client;
use CodeProject\Presenters\ClientPresenter;
/**
 * Description of ClientRepositoryEloquent
 *
 * @author laercio
 */
class ClientRepositoryEloquent extends BaseRepository implements ClientRespository
{
    protected $fieldSearchable =[
        'nome'
    ];


    public function model(){
        return Client::class;
    }
    
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

        /**
     * Informa que é usado presenter para o respositories
     * @return type
     */
     public function presenter() {
        return ClientPresenter::class;
    }
}
