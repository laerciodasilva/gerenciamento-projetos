<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeProject\Repositories\OauthClienteRepository;
use CodeProject\Entities\OauthCliente;

/**
 * Class OauthClienteRepositoryEloquent
 * @package namespace CodeProject\Repositories;
 */
class OauthClienteRepositoryEloquent extends BaseRepository implements OauthClienteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OauthCliente::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
