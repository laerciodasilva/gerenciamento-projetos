<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OauthClienteRepository
 * @package namespace CodeProject\Repositories;
 */
interface OauthClienteRepository extends RepositoryInterface
{
    //
}
