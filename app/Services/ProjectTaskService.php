<?php
/**
 * Aqui é criada toda à regra de negócio
 * Ex: enviar e-mail,desparar notificação, twittar ao inserir o novo e-mail
 */
namespace CodeProject\Services;

use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Validators\ProjectTaskValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
/**
 * Description of ProjectNoteService
 *
 * @author laercio
 */
class ProjectTaskService {
    
    /**
     *
     * @var ProjectTaskRepository 
     */
    protected $repository;
    
    /**
     *
     * @var ProjectRepository 
     */
    protected $projectRepository;
    
    /**
     *
     * @var ProjectTaskValidator 
     */
    protected $validator;


    /**
     * 
     * @param ProjectTaskRepository $repository
     * @param ProjectRepository $ProjectRepository
     * @param ProjectTaskValidator $validator
     */
    
    public function __construct(ProjectTaskRepository $repository, ProjectRepository $projetcRepository ,ProjectTaskValidator  $validator) {
        $this->repository = $repository;
        $this->projectRepository = $projetcRepository;
        $this->validator = $validator;
    }
    
    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(Array $data){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $project = $this->projectRepository->skipPresenter()->find($data['project_id']);
            $task = $project->task()->create($data);
            return $task;
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(Array $data, $id){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            return $this->repository->update($data, $id);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
    public function delete($id)
    {
        $task = $this->repository->skipPresenter()->find($id);
        return $task->delete();
    }
}
