<?php
/**
 * Aqui é criada toda à regra de negócio
 * Ex: enviar e-mail,desparar notificação, twittar ao inserir o novo e-mail
 */
namespace CodeProject\Services;

use CodeProject\Repositories\ProjectMemberRepository;
use CodeProject\Validators\ProjectMemberValidator;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Description of ProjectNoteService
 *
 * @author laercio
 */
class ProjectMemberService {
    
    /**
     *
     * @var ProjectMemberRepository 
     */
    protected $repository;
    
    /**
     *
     * @var ProjectMemberValidator 
     */
    protected $validator;


    /**
     * 
     * @param ProjectMemberRepository $repository
     * @param ProjectMemberValidator $validator
     */
    
    public function __construct(ProjectMemberRepository $repository,ProjectMemberValidator  $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
    }
    
    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(Array $data){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
}
