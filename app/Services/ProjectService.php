<?php
/**
 * Aqui é criada toda regra de negócio
 * Ex: enviar e-mail,desparar notificação, twittar ao inserir o novo e-mail
 */
namespace CodeProject\Services;

use CodeProject\Repositories\ProjectRepository;
use CodeProject\Validators\ProjectValidator;
use Prettus\Validator\Exceptions\ValidatorException;


use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Filesystem\Filesystem;
/**
 * Description of ClientService
 *
 * @author laercio
 */
class ProjectService {
    
    /**
     *
     * @var ProjectRepository 
     */
    protected $repository;
    
    /**
     *
     * @var ProjectValidator 
     */
    protected $validator;
    
    /**
     *
     * @var FileSystem
     */
    protected $fileSystem;
    
    /**
     *
     * @var Storage 
     */
    protected $storage;


    /**
     * 
     * @param ProjectRepository $repository
     * @param ProjectRepository $validator
     */
    
    public function __construct(ProjectRepository $repository,ProjectValidator  $validator, Filesystem $fileSystem, Storage $storage) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->fileSystem = $fileSystem;
        $this->storage = $storage;
    }
    
    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(Array $data){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(Array $data, $id){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
    public function createFile(array $data){
        $project = $this->repository->skipPresenter()->find($data['project_id']);
        $projectFile = $project->projectFiles()->create($data);
        $this->storage->put($projectFile->id.'.'.$data['extensao'], $this->fileSystem->get($data['file']));
    }
    
    /**
     * Verifica se usuário logado é dono do projeto
     * @param type $projectId
     * @return type
     */
    public function checkProjectOwner($projectId) {
        //Retoran id do usuário logado
        $userId = \Authorizer::getResourceOwnerId();

        return $this->repository->isOwner($projectId, $userId);
    }

    /**
     * Verifica se o usuário é um membro do projeto
     * @param type $projectId
     * @return type
     */
    public function checkProjectMember($projectId) {
        //Retoran id do usuário logado
        $userId = \Authorizer::getResourceOwnerId();

        return $this->repository->hasMamber($projectId, $userId);
    }

    /**
     * Verifica permissão do dono de projeto e membro de projeto
     * @param type $projectId
     * @return boolean
     */
    public function checkProjectPermissions($projectId) {
        if ($this->checkProjectOwner($projectId) || $this->checkProjectMember($projectId)) {
            return true;
        }
        return false;
    }
}
