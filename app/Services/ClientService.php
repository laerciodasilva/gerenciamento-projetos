<?php
/**
 * Aqui é criada toda regra de negócio
 * Ex: enviar e-mail,desparar notificação, twittar ao inserir o novo e-mail
 */
namespace CodeProject\Services;

use CodeProject\Repositories\ClientRespository;
use CodeProject\Validators\ClientValidator;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Description of ClientService
 *
 * @author laercio
 */
class ClientService {
    
    /**
     *
     * @var ClientRespository 
     */
    protected $repository;
    
    /**
     *
     * @var ClientValidator 
     */
    protected $validator;


    /**
     * 
     * @param ClientRespository $repository
     * @param ClientValidator $validator
     */
    
    public function __construct(ClientRespository $repository,ClientValidator  $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
    }
    
    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(Array $data){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(Array $data, $id){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
}
