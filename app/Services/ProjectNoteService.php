<?php
/**
 * Aqui é criada toda à regra de negócio
 * Ex: enviar e-mail,desparar notificação, twittar ao inserir o novo e-mail
 */
namespace CodeProject\Services;

use CodeProject\Repositories\ProjectNoteRepository;
use CodeProject\Validators\ProjectNoteValidator;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Description of ProjectNoteService
 *
 * @author laercio
 */
class ProjectNoteService {
    
    /**
     *
     * @var ProjectNoteRepository 
     */
    protected $repository;
    
    /**
     *
     * @var ProjectNoteValidator 
     */
    protected $validator;


    /**
     * 
     * @param ProjectNoteRepository $repository
     * @param ProjectNoteValidator $validator
     */
    
    public function __construct(ProjectNoteRepository $repository,ProjectNoteValidator  $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
    }
    
    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(Array $data){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
    
    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(Array $data, $id){
        try{
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
            
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }
}
