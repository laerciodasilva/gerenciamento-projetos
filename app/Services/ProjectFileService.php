<?php

/**
 * Aqui é criada toda à regra de negócio
 * Ex: enviar e-mail,desparar notificação, twittar ao inserir o novo e-mail
 */

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectFileRepository;
use CodeProject\Validators\ProjectFileValidator;
use Prettus\Validator\Exceptions\ValidatorException;
use CodeProject\Repositories\ProjectRepository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Prettus\Validator\Contracts\ValidatorInterface;

/**
 * Description of ProjectFileService
 *
 * @author laercio
 */
class ProjectFileService {

    /**
     *
     * @var ProjectFileRepository 
     */
    protected $repository;

    /**
     *
     * @var ProjectRepository 
     */
    protected $projectRepository;

    /**
     *
     * @var ProjectFileValidator 
     */
    protected $validator;

    /**
     *
     * @var type Filesystem
     */
    protected $filesystem;

    /**
     *
     * @var type Storage
     */
    protected $storage;

    /**
     * 
     * @param ProjectFileRepository $repository
     * @param ProjectFileValidator $validator
     */
    public function __construct(ProjectFileRepository $repository, ProjectFileValidator $validator, ProjectRepository $projectRepository, Filesystem $filesystem, Storage $storage) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->projectRepository = $projectRepository;
        $this->filesystem = $filesystem;
        $this->storage = $storage;
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(Array $data) {
        try {
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $project = $this->projectRepository->skipPresenter()->find($data['project_id']);
            $projectFile = $project->files()->create($data);

            $this->storage->put($projectFile->id . "." . $data['extensao'], $this->filesystem->get($data['file']));
            return $projectFile;
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(Array $data, $id) {
        try {
            //caso validação passar, cria e retorna operação
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            return $this->repository->update($data, $id);
        } catch (ValidatorException $ve) {
            return [
                'error' => true,
                'message' => $ve->getMessageBag()
            ];
        }
    }

    /**
     * Delete o arquivo no storage e no banco
     * @param type $id
     * @return type
     */
    public function delete($id) {
        $projectFile = $this->repository->skipPresenter()->find($id);
        if ($this->storage->exists($projectFile->getFileName())) {
            $this->storage->delete($projectFile->getFileName());
            $projectFile->delete();
        }
    }

    /**
     * Retorna o caminho do arquivo pesquisado por id do file
     * @param type $id
     * @return type
     */
    public function getFilePath($id) {
        $projectFile = $this->repository->skipPresenter()->find($id);
        return $this->getBaseUrl($projectFile);
    }

    /**
     * Retorna nome do arquivo
     * @param type $id
     * @return type
     */
    public function getFileName($id) {
        $projectFile = $this->repository->skipPresenter()->find($id);
        return $projectFile->getFileName();
    }

    /**
     * Retorn a ul
     * @param type $projectFile
     */
    public function getBaseUrl($projectFile) {
        $file = '';
        //Método abaixo verifica o drive usado. Ex: loca ou na nuvem


        switch ($this->storage->getDefaultDriver()) :
            case 'local':
                //O código (this->storage->getDriver()->getAdapter()->getPathPrefix()) retorn o prefixo do caminho do arquivo. Ex. storage/app
                $file = $this->storage->getDriver()->
                                getAdapter()->getPathPrefix() . $projectFile->getFileName();
                break;
        endswitch;

        return $file;
    }

    /**
     * Verifica se usuário logado é dono do projeto
     * @param type $projectId
     * @return type
     */
    public function checkProjectOwner($projectFileId) {
        //Retoran id do usuário logado
        $userId = \Authorizer::getResourceOwnerId();
        $projectId = $this->repository->skipPresenter()->find($projectFileId)->project_id;

        return $this->projectRepository->isOwner($projectId, $userId);
    }

    /**
     * Verifica se o usuário é um membro do projeto
     * @param type $projectId
     * @return type
     */
    public function checkProjectMember($projectFileId) {
        //Retoran id do usuário logado
        $userId = \Authorizer::getResourceOwnerId();

        $projectId = $this->repository->skipPresenter()->find($projectFileId)->project_id;

        return $this->projectRepository->hasMamber($projectId, $userId);
    }

    /**
     * Verifica permissão do dono de projeto e membro de projeto
     * @param type $projectId
     * @return boolean
     */
    public function checkProjectPermissions($projectFileId) {
        if ($this->checkProjectOwner($projectFileId) || $this->checkProjectMember($projectFileId)) {
            return true;
        }
        return false;
    }

}
