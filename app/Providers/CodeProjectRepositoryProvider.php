<?php

namespace CodeProject\Providers;

use Illuminate\Support\ServiceProvider;

class CodeProjectRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Primeiro paramétro é classe abstrata(que não pode ser instânciada)
         * Segundo paramétro é classe concreta(que pode ser instânciada)
         * Obs:Quando pretende que uma classe abstrata seja intânciada na dependência de injeção ou..., é preciso criar provider e, 
         * informe para laravel a classe que será instânciada
         */
        $this->app->bind(
        \CodeProject\Repositories\ClientRespository::class, 
        \CodeProject\Repositories\ClientRepositoryEloquent::class
        );
        
        $this->app->bind(
        \CodeProject\Repositories\ProjectRepository::class, 
         \CodeProject\Repositories\ProjectRepositoryEloquent::class
        );
        $this->app->bind(
        \CodeProject\Repositories\ProjectNoteRepository::class, 
         \CodeProject\Repositories\ProjectNoteRepositoryEloquent::class
        );
        
         $this->app->bind(
         \CodeProject\Repositories\ProjectMemberRepository::class, 
         \CodeProject\Repositories\ProjectMemberRepositoryEloquent::class
        );
         
         $this->app->bind(
         \CodeProject\Repositories\ProjectTaskRepository::class, 
         \CodeProject\Repositories\ProjectTaskRepositoryEloquent::class
        );
        
        $this->app->bind(
        \CodeProject\Repositories\ProjectFileRepository::class, 
         \CodeProject\Repositories\ProjectFileRepositoryEloquent::class
        );
        
        $this->app->bind(
           \CodeProject\Repositories\UserRepository::class, 
           \CodeProject\Repositories\UserRepositoryEloquent::class
        );
    }
}
