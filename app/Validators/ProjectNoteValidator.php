<?php

/**
 * Onde ficará toda à valição do project note
 */
namespace CodeProject\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Description of ProjectNoteValidator
 *
 * @author laercio
 */
class ProjectNoteValidator extends LaravelValidator{
    
    protected $rules = [
        'project_id' => 'required|integer',
        'titulo' => 'required',
        'anotacao' => 'required',
    ];
}
