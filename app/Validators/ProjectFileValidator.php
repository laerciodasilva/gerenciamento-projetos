<?php

/**
 * Onde ficará toda à valição do project note
 */
namespace CodeProject\Validators;

use Prettus\Validator\LaravelValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

/**
 * Description of ProjectNoteValidator
 *
 * @author laercio
 */
class ProjectFileValidator extends LaravelValidator{
    
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'project_id' => 'required|integer',
            'nome' => 'required',
            'file' => 'required|mimes:jpeg,jpg,JPG,png,gif,pdf,zip',
            'descricao' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'project_id' => 'required|integer',
            'nome' => 'required',
            'descricao' => 'required',
        ]
    ];
}
