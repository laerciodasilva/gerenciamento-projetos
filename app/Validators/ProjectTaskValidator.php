<?php

/**
 * Onde ficará toda à valição do project note
 */
namespace CodeProject\Validators;

use Prettus\Validator\LaravelValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
/**
 * Description of ProjectNoteValidator
 *
 * @author laercio
 */
class ProjectTaskValidator extends LaravelValidator{
    
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required',
            'project_id' => 'required|integer',
            'status' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required',
            'project_id' => 'required|integer',
            'start_date' => 'required',
            'due_date' => 'required',
            'status' => 'required',
        ]
    ];
}
