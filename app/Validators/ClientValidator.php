<?php

/**
 * Onde ficará toda à valição do cliente
 */
namespace CodeProject\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Description of ClientValidator
 *
 * @author laercio
 */
class ClientValidator extends LaravelValidator{
    
    protected $rules = [
        'nome' => 'required|max:255',
        'responsavel' => 'required|max:255',
        'email' => 'required|email',
        'telefone' => 'required',
        'endereco' => 'required',
    ];
}
