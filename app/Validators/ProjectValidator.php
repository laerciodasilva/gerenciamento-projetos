<?php

/**
 * Onde ficará toda à valição do porject
 */
namespace CodeProject\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Description of ProjectValidator
 *
 * @author laercio
 */
class ProjectValidator extends LaravelValidator{
    
    protected $rules = [
        'owner_id' => 'required|integer',
        'client_id' => 'required|integer',
        'nome' => 'required',
        'descricao' => 'required',
        'progresso' => 'required',
        'status' => 'required',
        'data_finalizacao' => 'required',
    ];
}
