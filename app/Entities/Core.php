<?php
namespace CodeProject\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Core
 *
 * @author laercio
 */
class Core  extends Model
{
    
    static function messageValidate($data) 
    {
        if(isset($data['error']) && $data['error'] == true) {
                $data  = json_decode($data['message'], true);
            $mensagem = null;
            foreach ($data as $k => $value) {
                foreach ($value as $v) {
                    if(is_null($mensagem))
                      $mensagem  .= $k;
                    else
                      $mensagem  .= ", ".$k;
                }
            }
            return $mensagem;
        }
    }
}
