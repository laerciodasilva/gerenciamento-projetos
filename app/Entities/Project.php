<?php

namespace CodeProject\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Project extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'owner_id',
        'client_id',
        'nome',
        'descricao',
        'progresso',
        'status',
        'data_finalizacao',
    ];
    
    public function notes(){
        
         return $this->hasMany(ProjectNote::class);
    }
    /**
     * Projeto possui vário membros relacionamento muitos para muitos 
     * @return type
     */
    public function members(){
        
        return $this->belongsToMany(User::class,'project_members','project_id','member_id');
    }
    /**
     * Projeto possuí vário arquivos
     * @return type
     */
    public function files(){
        
        return $this->hasMany(ProjectFile::class);
    }
    
    /**
     * Projeto possuí várias tarefas
     * @return type
     */
    public function task()
    {
        return $this->hasMany(ProjectTask::class);
    }

        /**
     * Projeto pertence há um cliente
     * @return type
     */
    public function client(){
        
        return $this->belongsTo(Client::class,'client_id', 'id');
    }
    
    /**
     * Um projeto pertence a usuário
     * @return type
     */
    public function user(){
        
        return $this->belongsTo(User::class,'owner_id', 'id');
    }
}
