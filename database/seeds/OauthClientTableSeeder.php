<?php

use Illuminate\Database\Seeder;

class OauthClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //CodeProject\Entities\OauthCliente::truncate();
        DB::table('oauth_clients')->insert([
            'id' => 'app1',
            'secret' => 'secret',
            'name' => 'App rest full',
            'created_at' => date('Y-m-d H:i:s')
            
        ]);
        
    }
}
