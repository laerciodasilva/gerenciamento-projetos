<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(CodeProject\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(CodeProject\Entities\Client::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->name,
        'responsavel' => $faker->name,
        'email' => $faker->email,
        'telefone' => $faker->phoneNumber,
        'endereco' => $faker->address,
        'obs' => $faker->sentence,
    ];
});
$factory->define(CodeProject\Entities\Project::class, function (Faker\Generator $faker) {
    return [
        'owner_id' => rand(1, 5),
        'client_id' => rand(1, 5),
        'nome' => $faker->word,
        'descricao' => $faker->sentence,
        'progresso' => rand(1, 100),
        'status' => rand(1, 5),
        'data_finalizacao' => $faker->dateTime('now'),
    ];
});
$factory->define(CodeProject\Entities\ProjectTask::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'project_id' => rand(1, 5),
        'status' => rand(1, 5),
        'start_date' => $faker->dateTime('now'),
        'due_date' => $faker->dateTime('now'),
    ];
});
$factory->define(CodeProject\Entities\ProjectMember::class, function (Faker\Generator $faker) {
    return [
        'project_id' => rand(1, 5),
        'member_id' => rand(1, 5),
    ];
});
$factory->define(CodeProject\Entities\ProjectNote::class, function (Faker\Generator $faker) {
    return [
        'project_id' => rand(1, 10),
        'titulo' => $faker->word,
        'anotacao' => $faker->paragraph,
    ];
});
